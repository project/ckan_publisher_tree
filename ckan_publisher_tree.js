(function($) {

/**
* Attaches the tree behavior to the publisher tree widget form.
*/
Drupal.behaviors.ckanPublisherTree = {
  attach: function(context, settings) {
    // Bind the expand/contract button to slide toggle the list underneath.
    $('.ckan-publisher-tree-button', context).once('ckan-publisher-tree-button').click(function() {
      $(this).toggleClass('ckan-publisher-tree-collapsed');
      $(this).siblings('ul').slideToggle('fast');
    });

    var delay = (function() {
      var timer = 0;
      return function(callback, ms) {
          clearTimeout (timer);
          timer = setTimeout(callback, ms);
      };
    })();

    $('.ckan-publisher-tree', context).once('ckan-publisher-tree')
        .prepend('<p><input class="form-control ckan-publisher-tree-search" type="text" name="q" value="" placeholder="Start typing a name..."></p>')
        .keyup(function() {

            delay(function(){


            var keyword = $('.ckan-publisher-tree-search').val().toLowerCase();
            console.log(keyword);
            if (keyword.length === 0) {
                $('.ckan-publisher-tree-button').show();
                $('.ckan-publisher-tree-level .form-type-checkbox').show();
                $('.ckan-publisher-tree-level .ckan-publisher-tree-collapsed').parent().find('ul').hide();
            }
            else {
                $('.ckan-publisher-tree-button').hide();
                $('.field-widget-ckan-publisher-tree ul').show();
                $('.ckan-publisher-tree-level .form-type-checkbox').each(function () {
                    var label = $(this).children('label').first(),
                        index = label.text().toLowerCase().indexOf(keyword);
                    if (index < 0) {
                        $(this).hide();
                        $(label).html($(label).text());
                    }
                    else {
                        $(this).show();
                        var toReplace = label.text().substring(index, index + keyword.length);
                        $(label).html($(label).text().replace(toReplace, '<span class="highlight">' + toReplace + '</span>'));

                        if ($(this).parent().parent().prev().css('display') == 'none') {
                            $(this).parent().parent().prev().show(); // TODO: refactor this - call a function recursively for all ancestors

                        }
                    }
                });
            }



            }, 200 );


        });
  }
};
})(jQuery);
