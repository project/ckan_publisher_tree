<?php

/**
 * Implements hook_views_data_alter().
 *
 * Register a new Views filter for ckan_publisher_reference fields.
 */
function ckan_publisher_tree_views_data_alter(&$data) {
  foreach ($data as $table_name => $table_info) {
    // If we're examining a Field.
    if (strpos($table_name, 'field_data_field_') === 0) {
      // Retrieve data about it.
      $field_name = substr($table_name, strlen('field_data_'));
      $field_info = field_info_field($field_name);

      // If it's a ckan_publisher_reference field.
      if ($field_info['type'] == 'ckan_publisher_reference') {
        // Give it our custom filter.
        $data[$table_name][$field_name . '_id']['filter']['handler'] = 'ckan_publisher_tree_views_handler_filter_ckan_publisher_reference';
      }
    }
  }
}

