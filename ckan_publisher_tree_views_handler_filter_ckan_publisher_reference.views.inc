<?php

/**
 * @file
 * Definition of ckan_publisher_tree_views_handler_filter_ckan_publisher_reference.
 */

/**
 * Filter allows the user to select from all rows in the current instance of a ckan_publisher_reference field.
 *
 * @ingroup views_filter_handlers
 */
class ckan_publisher_tree_views_handler_filter_ckan_publisher_reference extends views_handler_filter_in_operator {
  /**
   * Pre-populate options drop-down with all rows currently in use.
   */
  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }
    if ($this->ensure_my_table()) {
      $query = db_select($this->table_alias, 'ta')
        ->distinct()
        ->fields('ta', array($this->real_field))
        ->fields('c', array('title'))
        ->orderBy('title');
      $query->innerJoin('ckan_publisher', 'c', "ta.{$this->real_field} = c.id");
      return $this->value_options = $query->execute()->fetchAllKeyed();
    }
    return $this->value_options = array();
  }
}

